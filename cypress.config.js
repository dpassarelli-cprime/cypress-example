const { defineConfig } = require("cypress")

module.exports = defineConfig({
  e2e: {
    /**
     * Set this to the base url of the local development server.
     */
    baseUrl: 'https://example.cypress.io',
    env: {
      /**
       * Set this to the default identity to use for authentication.
       */
      LOCAL_LOGIN_ID: 'test-id'
    },
    setupNodeEvents: (on, config) => {
      on('task', {
        /**
         * This task enables the output of "log" to appear in the CLI
         * when running the tests in headless mode.
         */
        log: (msg) => {
          console.log(msg)
          return null // this is required; do not remove
        }
      })
    }
  }
})
