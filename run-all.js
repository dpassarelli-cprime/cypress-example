const cypress = require('cypress')

cypress.run({
  spec: 'cypress/e2e/*.cy.js',
  reporter: 'junit',
  browser: 'chrome',
  testingType: 'e2e',
  config: {
    video: false
  },
  headless: true,
  exit: true
})
  .then((result) => {
    /**
     * Even if the promise resolves, it does not automatically mean
     * everything ran as expected. If there was a problem, the CLI
     * should clearly communicate that; therefore, the following cases
     * will return a non-zero exit code:
     */
    if (result.status === 'failed') {
      // this means that the tests were unable to be run (not found,
      // runtime error, etc)
      process.exit(1)
    }

    if (result.totalFailed > 0) {
      // this means the tests were run, but at least one did not pass
      process.exit(result.totalFailed)
    }
  })
  .catch((err) => {
    console.error(err)
    process.exit(1)
  })
