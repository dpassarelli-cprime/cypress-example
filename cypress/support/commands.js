// ***********************************************
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

/**
 * In order to display the output from `cy.log`, the default
 * behavior of that command needs to be overwritten (refer to
 * cypress.config.js for the implementation).
 */
Cypress.Commands.overwrite('log', (subject, message) => {
  cy.task('log', message)
})

/**
 * This command can be run before tests to ensure that the
 * web browser can access secured endpoints.
 */
Cypress.Commands.add('login', (id) => {
  cy.log(`LOG: Logging in as ${id}`)

  // cy.request({
  //   method: 'POST',
  //   url: '/aurora/int/MainServlet', // relative to baseUrl defined in cypress.config.js
  //   body: {
  //     controllerId: 'LoginController',
  //     methodId: 'login',
  //     applicationId: 'common',
  //     loginid: id
  //   }
  // }).then(...save to cookie?)
})
