describe('submitting a new comment', () => {
  before(() => {
    cy.login(Cypress.env('LOCAL_LOGIN_ID'))
  })

  it('contains the correct request data', () => {
    cy.visit('/commands/network-requests')

    cy.get('button.network-put').click()

    cy.intercept('https://jsonplaceholder.cypress.io/comments/*').as('submitNewComment')

    cy
      .wait('@submitNewComment')
      .should(({ request }) => {
        /** content type **/
        let expected = /^application\/x-www-form-urlencoded(; charset=utf-8)?$/i
        let actual = request.headers['content-type']
        expect(actual).to.match(expected)

        const decodedRequestBody = new URLSearchParams(request.body)

        /** request body: name **/
        expected = 'Using PUT in cy.intercept()'
        actual = decodedRequestBody.get('name')
        expect(actual).to.equal(expected)

        /** request body: email **/
        expected = 'hello@cypress.io'
        actual = decodedRequestBody.get('email')
        expect(actual).to.equal(expected)

        /** request body: body **/
        expected = 'You can change the method used for cy.intercept() to be GET, POST, PUT, PATCH, or DELETE'
        actual = decodedRequestBody.get('body')
        expect(actual).to.equal(expected)
      })
  })
})